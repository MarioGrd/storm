﻿namespace CogentCode.Storm.Management.Infrastructure.SingalR
{
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Port;

    public interface IStormHubAPI
    {
        Task SendAsync();
    }

    internal class StormHub: IStormHub
    {
        public async Task SendAsync(NoteRemovedNotification notification)
        {
            await Task.CompletedTask;
        }

        public async Task SendAsync(NoteLocationUpdatedNotification notification)
        {
            await Task.CompletedTask;
        }

        public async Task SendAsync(NoteAddedNotification notification)
        {
            await Task.CompletedTask;
        }
    }
}
