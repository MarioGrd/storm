﻿namespace CogentCode.Storm.Management.Infrastructure.SingalR
{
    using System;

    using CogentCode.Storm.Management.Port;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class SignalModule
    {
        public static IServiceCollection AddSignalRModule(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            SignalRSettings? settings = configuration.GetSection("SignalR").Get<SignalRSettings>();

            if (settings == null)
            {
                throw new ApplicationException($"Unable to find '{nameof(SignalRSettings)}'.");
            }

            _ = services.AddScoped<IStormHub, StormHub>();

            return services;
        }
    }

    public class SignalRSettings
    {
        public string? Url { get; set; }
    }
}
