﻿namespace CogentCode.Storm.Management.Domain.StormAggregate
{
    using System;

    using CogentCode.Seedwork;

    public class StormId : IKey
    {
        private const string Prefix = "Storm-";

        public StormId(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; private set; }

        public string AsString() => $"{Prefix}{this.Id}";

        public override string ToString() => this.AsString();

        public override bool Equals(object? obj)
            => obj != null && obj is StormId stromId && Equals(this.Id, stromId.Id);

        public override int GetHashCode() => this.Id.GetHashCode();
    }
}
