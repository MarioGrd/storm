﻿namespace CogentCode.Storm.Management.Domain.StormAggregate.Events
{
    using System;

    using CogentCode.Seedwork;

    public class NoteAddedEvent : DomainEvent<StormId>
    {
        public NoteAddedEvent(
            Guid noteId,
            string name,
            NoteType type,
            long x,
            long y,
            StormId aggregateId) : base(aggregateId)
        {
            this.NoteId = noteId;
            this.Name = name;
            this.Type = type;
            this.X = x;
            this.Y = y;
        }

        public Guid NoteId { get; }

        public string Name { get; }

        public NoteType Type { get; }

        public long X { get; }

        public long Y { get; }
    }
}
