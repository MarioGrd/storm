﻿namespace CogentCode.Storm.Management.Domain.StormAggregate.Events
{
    using System;

    using CogentCode.Seedwork;

    public class NoteLocationUpdatedEvent : DomainEvent<StormId>
    {
        public NoteLocationUpdatedEvent(
            Guid noteId,
            long x,
            long y,
            StormId aggregateId) : base(aggregateId)
        {
            this.NoteId = noteId;
            this.X = x;
            this.Y = y;
        }

        public Guid NoteId { get; }

        public long X { get; }

        public long Y { get; }
    }
}
