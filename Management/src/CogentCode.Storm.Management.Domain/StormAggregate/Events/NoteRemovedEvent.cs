﻿namespace CogentCode.Storm.Management.Domain.StormAggregate.Events
{
    using System;

    using CogentCode.Seedwork;

    public class NoteRemovedEvent : DomainEvent<StormId>
    {
        public NoteRemovedEvent(
            Guid noteId,
            StormId aggregateId) : base(aggregateId)
        {
            this.NoteId = noteId;
        }

        public Guid NoteId { get; }
    }
}
