﻿namespace CogentCode.Storm.Management.Domain.StormAggregate.Events
{
    using System;

    using CogentCode.Seedwork;

    public class StormCreatedEvent : DomainEvent<StormId>
    {
        public StormCreatedEvent(
            StormId aggregateId,
            Guid teamId,
            long createdAt,
            string name) : base(aggregateId)
        {
            this.TeamId = teamId;
            this.CreatedAt = createdAt;
            this.Name = name;
        }

        public Guid TeamId { get; }

        public long CreatedAt { get; }

        public string Name { get; }
    }
}
