﻿namespace CogentCode.Storm.Management.Domain.StormAggregate
{
    using System;
    using System.Collections.Generic;

    using CogentCode.Seedwork;

    public class Note : ValueObject
    {
        public Note(
            Guid id,
            string name,
            NoteType type,
            long x,
            long y)
        {
            this.Id = id;
            this.Name = name;
            this.Type = type;
            this.X = x;
            this.Y = y;
        }

        public Guid Id { get; }

        public string Name { get; }

        public NoteType Type { get; }

        public long X { get; }

        public long Y { get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.Id;
            yield return this.Name;
            yield return this.Type;
            yield return this.X;
            yield return this.Y;
        }
    }
}

