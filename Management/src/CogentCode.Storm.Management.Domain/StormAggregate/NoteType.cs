﻿namespace CogentCode.Storm.Management.Domain.StormAggregate
{
    using CogentCode.Storm.Seedwork;

    public class NoteType : Enumeration
    {
        public NoteType(int id, string name) : base(id, name)
        {
        }

        public NoteType() : base()
        {
        }

        public static NoteType COMMAND = new NoteType(1, nameof(COMMAND));
        public static NoteType EVENT = new NoteType(2, nameof(EVENT));
        public static NoteType AGGREGATE = new NoteType(3, nameof(AGGREGATE));
        public static NoteType PROCESS = new NoteType(4, nameof(PROCESS));
    }
}
