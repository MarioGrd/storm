﻿namespace CogentCode.Storm.Management.Domain.StormAggregate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CogentCode.Seedwork;
    using CogentCode.Storm.Management.Domain.StormAggregate.Events;

    public class Storm : EventAggregateRoot<StormId>
    {
        public Storm(
            Guid aggregateId,
            Guid teamId,
            string name,
            long createdAt)
        {
            this.Id = new StormId(aggregateId);
            this.TeamId = teamId;
            this.Name = name;
            this.CreatedAt = createdAt;

            this.AddDomainEvent(new StormCreatedEvent(
                this.Id,
                this.TeamId,
                this.CreatedAt,
                this.Name));
        }

        public Storm(ICollection<IDomainEvent<StormId>> events) : base(events)
        {
            this.Id = new StormId(Guid.Empty);
            this.Name = string.Empty;
        }

        public override StormId Id { get; protected set; }

        public Guid TeamId { get; protected set; }

        public string Name { get; protected set; }

        public long CreatedAt { get; protected set; }

        public ICollection<Note> Notes { get; } = new List<Note>();

        public void AddNote(Note note)
        {
            Note? existingNote = this.GetNoteById(note.Id);

            if (existingNote != null)
            {
                return;
            }

            this.Notes.Add(note);

            this.AddDomainEvent(new NoteAddedEvent(
                note.Id,
                note.Name,
                note.Type,
                note.X,
                note.Y,
                this.Id));
        }

        public void RemoveNote(Guid noteId)
        {
            Note? note = this.GetNoteById(noteId);

            if (note == null)
            {
                return;
            }

            this.Notes.Remove(note);

            this.AddDomainEvent(new NoteRemovedEvent(noteId, this.Id));
        }

        public void UpdateNoteLocation(Guid noteId, long x, long y)
        {
            Note? note = this.GetNoteById(noteId);

            if (note == null)
            {
                return;
            }

            this.Notes.Remove(note);
            this.Notes.Add(new Note(note.Id, note.Name, note.Type, x, y));

            this.AddDomainEvent(new NoteLocationUpdatedEvent(noteId, x, y, this.Id));
        }

        public void On(StormCreatedEvent @event)
        {
            this.Id = @event.AggregateId;
            this.CreatedAt = @event.CreatedAt;
            this.Name = @event.Name;
            this.TeamId = @event.TeamId;
        }

        public void On(NoteAddedEvent @event)
        {
            Note? note = this.GetNoteById(@event.NoteId);

            if (note != null)
            {
                return;
            }

            this.Notes.Add(new Note(
                @event.NoteId,
                @event.Name,
                @event.Type,
                @event.X,
                @event.Y));
        }

        public void On(NoteRemovedEvent @event)
        {
            Note? note = this.GetNoteById(@event.NoteId);

            if (note == null)
            {
                return;
            }

            this.Notes.Remove(note);
        }

        public void On(NoteLocationUpdatedEvent @event)
        {
            Note? note = this.GetNoteById(@event.NoteId);

            if (note == null)
            {
                return;
            }

            this.Notes.Remove(note);
            this.Notes.Add(new Note(note.Id, note.Name, note.Type, @event.X, @event.Y));
        }

        private Note? GetNoteById(Guid noteId) => this.Notes.Where(n => n.Id == noteId).SingleOrDefault();
    }
}
