﻿namespace CogentCode.Storm.Management.Domain.TeamAggregate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CogentCode.Seedwork;

    // TODO: Figure out how to have Value Object as Key for EF Core.
    public class Team : Entity<Guid>
    {
        public Team(
            Guid id,
            string name,
            long createdAt)
        {
            this.Id = id;
            this.Name = name;
            this.CreatedAt = createdAt;
            this.Members = new List<TeamMember>();
            this.Storms = new List<TeamStorm>();
        }

        public override Guid Id { get; protected set; }

        public string Name { get; protected set; }

        public long CreatedAt { get; protected set; }

        public List<TeamMember> Members { get; protected set; }

        public List<TeamStorm> Storms { get; protected set; }

        public void AddTeamMember(TeamMember member)
        {
            TeamMember? existingMember = this.GetMember(member);

            if (existingMember == null)
            {
                this.Members.Add(member);
            }
        }

        public void RemoveTeamMember(TeamMember member)
        {
            TeamMember? existingMember = this.GetMember(member);

            if (existingMember != null)
            {
                this.Members.Remove(existingMember);
            }
        }

        public void AddTeamStorm(TeamStorm storm)
        {
            TeamStorm? existingStorm = this.GetStorm(storm);

            if (existingStorm == null)
            {
                this.Storms.Add(storm);
            }
        }

        public void RemoveTeamStorm(TeamStorm storm)
        {
            TeamStorm? existingStorm = this.GetStorm(storm);

            if (existingStorm != null)
            {
                this.Storms.Remove(existingStorm);
            }
        }

        private TeamMember? GetMember(TeamMember member) => this.Members.Where(tm => tm == member).FirstOrDefault();

        private TeamStorm? GetStorm(TeamStorm storm) => this.Storms.Where(ts => ts == storm).FirstOrDefault();
    }
}
