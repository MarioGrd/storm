﻿namespace CogentCode.Storm.Management.Domain.TeamAggregate
{
    using System;
    using System.Collections.Generic;

    using CogentCode.Seedwork;

    public class TeamMember : ValueObject
    {
        public TeamMember(Guid teamId, Guid userId)
        {
            this.TeamId = teamId;
            this.UserId = userId;
        }

        public Guid TeamId { get; protected set; }

        public Guid UserId { get; protected set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.TeamId;
            yield return this.UserId;
        }
    }
}
