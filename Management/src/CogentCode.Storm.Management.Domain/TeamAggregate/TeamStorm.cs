﻿namespace CogentCode.Storm.Management.Domain.TeamAggregate
{
    using System;
    using System.Collections.Generic;

    using CogentCode.Seedwork;

    public class TeamStorm : ValueObject
    {
        public TeamStorm(
            Guid teamId,
            Guid stormId)
        {
            this.TeamId = teamId;
            this.StormId = stormId;
        }

        public Guid TeamId { get; protected set; }

        public Guid StormId { get; protected set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.TeamId;
            yield return this.StormId;
        }
    }
}
