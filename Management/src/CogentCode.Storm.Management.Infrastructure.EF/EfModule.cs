﻿namespace CogentCode.Storm.Management.Infrastructure.EF
{
    using System;
    using System.Reflection;

    using CogentCode.Storm.Management.Infrastructure.EF.Core;
    using CogentCode.Storm.Management.Infrastructure.EF.Repository;
    using CogentCode.Storm.Management.Port;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class EfModule
    {
        public static IServiceCollection AddEfModule(
           this IServiceCollection services,
           IConfiguration configuration)
        {
            EfSettings? settings = configuration.GetSection("Ef").Get<EfSettings>();

            if (settings == null)
            {
                throw new ApplicationException($"Unable to find '{nameof(EfSettings)}' settings.");
            }

            _ = services.AddDbContext<TeamContext>(options => options.UseSqlServer(
                    settings.ConnectionString,
                    config => config.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName))
                );

            _ = services.AddScoped<ITeamRepository, TeamRepository>();
            _ = services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }

    public class EfSettings
    {
        public string? ConnectionString { get; set; }
    }
}
