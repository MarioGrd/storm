﻿namespace CogentCode.Storm.Management.Infrastructure.EF.Repository
{
    using System;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;
    using CogentCode.Storm.Management.Infrastructure.EF.Core;
    using CogentCode.Storm.Management.Port;

    using Microsoft.EntityFrameworkCore;

    internal class TeamRepository : ITeamRepository
    {
        private readonly DbSet<Team> teams;

        public TeamRepository(TeamContext context)
        {
            this.teams = context.Set<Team>();
        }

        public async Task AddTeamAsync(Team team)
        {
            await this.teams.AddAsync(team);
        }

        public async Task<Team> GetTeamByIdSafeAsync(Guid id)
        {
            Team? team = await this.teams.FindAsync(id);

            return team ?? throw new ApplicationException("Not found.");
        }

        public void UpdateTeam(Team team)
        {
            this.teams.Update(team);
        }
    }
}
