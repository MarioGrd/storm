﻿// <auto-generated />
using System;
using CogentCode.Storm.Management.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CogentCode.Storm.Management.Infrastructure.EF.Migrations
{
    [DbContext(typeof(TeamContext))]
    partial class TeamContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CogentCode.Storm.Management.Domain.TeamAggregate.Team", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<long>("CreatedAt")
                        .HasColumnType("bigint");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Team");
                });

            modelBuilder.Entity("CogentCode.Storm.Management.Domain.TeamAggregate.Team", b =>
                {
                    b.OwnsMany("CogentCode.Storm.Management.Domain.TeamAggregate.TeamMember", "Members", b1 =>
                        {
                            b1.Property<Guid>("TeamId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<int>("Id")
                                .ValueGeneratedOnAdd()
                                .HasColumnType("int")
                                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                            b1.Property<Guid>("UserId")
                                .HasColumnType("uniqueidentifier");

                            b1.HasKey("TeamId", "Id");

                            b1.ToTable("TeamMembers");

                            b1.WithOwner()
                                .HasForeignKey("TeamId");
                        });

                    b.OwnsMany("CogentCode.Storm.Management.Domain.TeamAggregate.TeamStorm", "Storms", b1 =>
                        {
                            b1.Property<Guid>("TeamId")
                                .HasColumnType("uniqueidentifier");

                            b1.Property<int>("Id")
                                .ValueGeneratedOnAdd()
                                .HasColumnType("int")
                                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                            b1.Property<Guid>("StormId")
                                .HasColumnType("uniqueidentifier");

                            b1.HasKey("TeamId", "Id");

                            b1.ToTable("TeamStorms");

                            b1.WithOwner()
                                .HasForeignKey("TeamId");
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
