﻿namespace CogentCode.Storm.Management.Infrastructure.EF.Core
{
    using System.Reflection;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;

    public class TeamContext : DbContext
    {
        public TeamContext(DbContextOptions<TeamContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
            => modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        // TODO: See what can be done about this.
        public class TeamDbContextDesingFactory : IDesignTimeDbContextFactory<TeamContext>
        {
            public TeamContext CreateDbContext(string[] args)
            {
                DbContextOptionsBuilder<TeamContext> optionsBuilder = new DbContextOptionsBuilder<TeamContext>();

                optionsBuilder
                    .UseSqlServer("Server=.; Database=Storm;Trusted_Connection=True;MultipleActiveResultSets=true");

                return new TeamContext(optionsBuilder.Options);
            }
        }
    }
}
