﻿
namespace CogentCode.Storm.Management.Infrastructure.EF.Core
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Port;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly TeamContext context;

        public UnitOfWork(TeamContext context)
        {
            this.context = context;
        }

        public async Task SaveChangesAsync(CancellationToken token = default)
        {
            await this.context.SaveChangesAsync(token);
        }
    }
}
