﻿namespace CogentCode.Storm.Management.Infrastructure.EF.Configuration
{
    using CogentCode.Storm.Management.Domain.TeamAggregate;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class TeamEntityTypeConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.OwnsMany(t => t.Members, onb =>
            {
                onb.WithOwner().HasForeignKey(fk => fk.TeamId);
                onb.ToTable("TeamMembers");
            });

            builder.OwnsMany(t => t.Storms, onb =>
            {
                onb.WithOwner().HasForeignKey(fk => fk.TeamId);
                onb.ToTable("TeamStorms");
            });
        }
    }
}
