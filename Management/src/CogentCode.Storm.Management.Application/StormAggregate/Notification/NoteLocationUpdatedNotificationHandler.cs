﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Notification
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class NoteLocationUpdatedNotificationHandler : INotificationHandler<NoteLocationUpdatedNotification>
    {
        private readonly IStormHub stormHub;

        public NoteLocationUpdatedNotificationHandler(IStormHub stormHub)
        {
            this.stormHub = stormHub;
        }
        public async Task Handle(NoteLocationUpdatedNotification notification, CancellationToken cancellationToken)
        {
            await this.stormHub.SendAsync(notification);
        }
    }
}
