﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Notification
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class NoteAddedNotificationHandler : INotificationHandler<NoteAddedNotification>
    {
        private readonly IStormHub stormHub;

        public NoteAddedNotificationHandler(IStormHub stormHub)
        {
            this.stormHub = stormHub;
        }

        public async Task Handle(NoteAddedNotification notification, CancellationToken cancellationToken)
        {
            await this.stormHub.SendAsync(notification);
        }
    }
}
