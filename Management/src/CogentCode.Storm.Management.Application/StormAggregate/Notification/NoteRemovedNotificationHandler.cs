﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Notification
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class NoteRemovedNotificationHandler : INotificationHandler<NoteRemovedNotification>
    {
        private readonly IStormHub stormHub;

        public NoteRemovedNotificationHandler(IStormHub stormHub)
        {
            this.stormHub = stormHub;
        }

        public async Task Handle(NoteRemovedNotification notification, CancellationToken cancellationToken)
        {
            await this.stormHub.SendAsync(notification);
        }
    }
}
