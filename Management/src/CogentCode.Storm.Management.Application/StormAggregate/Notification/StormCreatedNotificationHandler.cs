﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Notification
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamStorm;
    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class StormCreatedNotificationHandler : INotificationHandler<StormCreatedNotification>
    {
        private readonly IMediator mediator;

        public StormCreatedNotificationHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async Task Handle(StormCreatedNotification notification, CancellationToken cancellationToken)
        {
            await this.mediator.Send(new AddTeamStormCommand(notification.AggregateId, notification.TeamId));
        }
    }
}
