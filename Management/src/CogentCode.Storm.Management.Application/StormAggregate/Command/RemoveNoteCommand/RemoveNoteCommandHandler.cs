﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.RemoveNoteCommand
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Seedwork.EventBus;
    using CogentCode.Storm.Management.Domain.StormAggregate;

    using MediatR;

    internal class RemoveNoteCommandHandler : AsyncRequestHandler<RemoveNoteCommand>
    {
        private readonly IEventRepository<Storm, StormId> repository;

        public RemoveNoteCommandHandler(IEventRepository<Storm, StormId> repository)
        {
            this.repository = repository;
        }

        protected override async Task Handle(RemoveNoteCommand request, CancellationToken cancellationToken)
        {
            Storm storm = await this.repository.GetByIdAsync(new StormId(request.SessionId));

            storm.RemoveNote(request.NoteId);

            await this.repository.SaveAsync(storm);
        }
    }
}
