﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.RemoveNoteCommand
{
    using System;

    using MediatR;

    public class RemoveNoteCommand : IRequest
    {
        public RemoveNoteCommand(
            Guid sessionId,
            Guid noteId)
        {
            this.SessionId = sessionId;
            this.NoteId = noteId;
        }

        public Guid SessionId { get; }

        public Guid NoteId { get; }
    }
}
