﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.RemoveNoteCommand
{
    using FluentValidation;

    public class RemoveNoteCommandValidator : AbstractValidator<RemoveNoteCommand>
    {
        public RemoveNoteCommandValidator()
        {
            this.RuleFor(r => r.SessionId).NotEmpty();
            this.RuleFor(r => r.NoteId).NotEmpty();
        }
    }
}
