﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.CreateStormCommand
{
    using System;

    using MediatR;

    public class CreateStormCommand : IRequest
    {
        public CreateStormCommand(
            string name,
            Guid productId)
        {
            this.Name = name;
            this.ProductId = productId;
        }

        public string Name { get; }

        public Guid ProductId { get; }
    }
}
