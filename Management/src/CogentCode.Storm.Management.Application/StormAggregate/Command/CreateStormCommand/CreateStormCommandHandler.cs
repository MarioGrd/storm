﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.CreateStormCommand
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Seedwork.EventBus;
    using CogentCode.Storm.Management.Domain.StormAggregate;

    using MediatR;

    internal class CreateStormCommandHandler : AsyncRequestHandler<CreateStormCommand>
    {
        private readonly IEventRepository<Storm, StormId> repository;

        public CreateStormCommandHandler(
            IEventRepository<Storm, StormId> repository)
        {
            this.repository = repository;
        }

        protected override async Task Handle(CreateStormCommand request, CancellationToken cancellationToken)
        {
            Storm storm = new Storm(
                Guid.NewGuid(),
                request.ProductId,
                request.Name,
                DateTimeOffset.UtcNow.Ticks);

            await this.repository.SaveAsync(storm);
        }
    }
}
