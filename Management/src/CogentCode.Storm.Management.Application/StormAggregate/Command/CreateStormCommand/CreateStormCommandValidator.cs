﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.CreateStormCommand
{
    using FluentValidation;

    public class CreateStormCommandValidator : AbstractValidator<CreateStormCommand>
    {
        public CreateStormCommandValidator()
        {
            this.RuleFor(r => r.ProductId).NotEmpty();
            this.RuleFor(r => r.Name).NotEmpty();
        }
    }
}
