﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.UpdateNoteLocationCommand
{

    using FluentValidation;

    public class UpdateNoteLocationCommandValidator : AbstractValidator<UpdateNoteLocationCommand>
    {
        public UpdateNoteLocationCommandValidator()
        {
            this.RuleFor(r => r.NoteId).NotEmpty();
            this.RuleFor(r => r.SessionId).NotEmpty();
            this.RuleFor(r => r.X).GreaterThan(0);
            this.RuleFor(r => r.Y).GreaterThan(0);
        }
    }
}
