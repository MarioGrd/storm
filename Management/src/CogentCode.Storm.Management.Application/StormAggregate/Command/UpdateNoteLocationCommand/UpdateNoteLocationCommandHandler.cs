﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.UpdateNoteLocationCommand
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Seedwork.EventBus;
    using CogentCode.Storm.Management.Domain.StormAggregate;

    using MediatR;

    internal class UpdateNoteLocationCommandHandler : AsyncRequestHandler<UpdateNoteLocationCommand>
    {
        private readonly IEventRepository<Storm, StormId> repository;

        public UpdateNoteLocationCommandHandler(IEventRepository<Storm, StormId> repository)
        {
            this.repository = repository;
        }

        protected override async Task Handle(UpdateNoteLocationCommand request, CancellationToken cancellationToken)
        {
            Storm strom = await this.repository.GetByIdAsync(new StormId(request.SessionId));

            strom.UpdateNoteLocation(request.NoteId, request.X, request.Y);

            await this.repository.SaveAsync(strom);
        }
    }
}
