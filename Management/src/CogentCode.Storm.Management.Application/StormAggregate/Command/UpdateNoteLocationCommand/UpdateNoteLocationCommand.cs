﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.UpdateNoteLocationCommand
{
    using System;

    using MediatR;

    public class UpdateNoteLocationCommand : IRequest
    {
        public UpdateNoteLocationCommand(Guid sessionId, Guid noteId, long x, long y)
        {
            this.SessionId = sessionId;
            this.NoteId = noteId;
            this.X = x;
            this.Y = y;
        }

        public Guid SessionId { get; }

        public Guid NoteId { get; }

        public long X { get; }

        public long Y { get; }
    }
}
