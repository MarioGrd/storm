﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.AddNoteCommand
{
    using System;

    using CogentCode.Storm.Management.Domain.StormAggregate;
    using CogentCode.Storm.Seedwork;

    using MediatR;

    public class AddNoteCommand : IRequest
    {
        public AddNoteCommand(
            Guid sessionId,
            long x,
            long y,
            string name,
            int type)
        {
            this.SessionId = sessionId;
            this.X = x;
            this.Y = y;
            this.Name = name;
            this.Type = Enumeration.FromValue<NoteType>(type);
        }

        public Guid SessionId { get; }

        public long X { get; }

        public long Y { get; }

        public string Name { get; }

        public NoteType Type { get; }
    }
}
