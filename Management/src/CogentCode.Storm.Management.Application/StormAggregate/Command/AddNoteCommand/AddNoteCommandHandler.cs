﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.AddNoteCommand
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Seedwork.EventBus;
    using CogentCode.Storm.Management.Domain.StormAggregate;

    using MediatR;

    internal class AddNoteCommandHandler : AsyncRequestHandler<AddNoteCommand>
    {
        private readonly IEventRepository<Storm, StormId> repository;

        public AddNoteCommandHandler(IEventRepository<Storm, StormId> stormRepository)
        {
            this.repository = stormRepository;
        }

        protected override async Task Handle(AddNoteCommand request, CancellationToken cancellationToken)
        {
            Storm storm = await this.repository.GetByIdAsync(new StormId(request.SessionId));

            storm.AddNote(new Note(
                Guid.NewGuid(),
                request.Name,
                request.Type,
                request.X,
                request.Y));

            await this.repository.SaveAsync(storm);
        }
    }
}
