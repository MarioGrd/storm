﻿namespace CogentCode.Storm.Management.Application.StormAggregate.Command.AddNoteCommand
{

    using FluentValidation;

    public class AddNoteCommandValidator : AbstractValidator<AddNoteCommand>
    {
        public AddNoteCommandValidator()
        {
            this.RuleFor(r => r.SessionId).NotEmpty();
            this.RuleFor(r => r.X).GreaterThan(0);
            this.RuleFor(r => r.Y).GreaterThan(0);
            this.RuleFor(r => r.Name).NotEmpty();
        }
    }
}
