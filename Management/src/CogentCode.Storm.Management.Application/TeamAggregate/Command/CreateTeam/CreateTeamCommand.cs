﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.CreateTeam
{
    using MediatR;

    public class CreateTeamCommand : IRequest
    {
        public CreateTeamCommand(string name)
        {
            this.Name = name;
        }

        public string Name { get; }
    }
}
