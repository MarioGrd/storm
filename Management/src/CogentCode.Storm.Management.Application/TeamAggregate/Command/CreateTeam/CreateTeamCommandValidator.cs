﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.CreateTeam
{
    using FluentValidation;

    public class CreateTeamCommandValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamCommandValidator()
        {
            this.RuleFor(r => r.Name).NotEmpty();
        }
    }
}
