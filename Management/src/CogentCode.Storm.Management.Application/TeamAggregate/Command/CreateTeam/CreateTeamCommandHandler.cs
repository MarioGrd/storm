﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.CreateTeam
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;
    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class CreateTeamCommandHandler : AsyncRequestHandler<CreateTeamCommand>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ITeamRepository teamRepository;

        public CreateTeamCommandHandler(IUnitOfWork unitOfWork, ITeamRepository teamRepository)
        {
            this.unitOfWork = unitOfWork;
            this.teamRepository = teamRepository;
        }

        protected override async Task Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            Team team = new Team(
                Guid.NewGuid(),
                request.Name,
                DateTimeOffset.UtcNow.Ticks);

            await this.teamRepository.AddTeamAsync(team);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}
