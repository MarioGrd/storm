﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamStorm
{
    using System;

    using MediatR;

    public class RemoveTeamStormCommand : IRequest
    {
        public RemoveTeamStormCommand(Guid teamId, Guid stormId)
        {
            this.TeamId = teamId;
            this.StormId = stormId;
        }

        public Guid TeamId { get; }

        public Guid StormId { get; }
    }
}
