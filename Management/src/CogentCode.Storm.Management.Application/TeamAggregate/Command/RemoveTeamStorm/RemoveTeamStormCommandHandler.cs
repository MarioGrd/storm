﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamStorm
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;
    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class RemoveTeamStormCommandHandler : AsyncRequestHandler<RemoveTeamStormCommand>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ITeamRepository teamRepository;

        public RemoveTeamStormCommandHandler(IUnitOfWork unitOfWork, ITeamRepository teamRepository)
        {
            this.unitOfWork = unitOfWork;
            this.teamRepository = teamRepository;
        }

        protected override async Task Handle(RemoveTeamStormCommand request, CancellationToken cancellationToken)
        {
            Team team = await this.teamRepository.GetTeamByIdSafeAsync(request.TeamId);

            team.RemoveTeamStorm(new TeamStorm(request.TeamId, request.StormId));

            this.teamRepository.UpdateTeam(team);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}
