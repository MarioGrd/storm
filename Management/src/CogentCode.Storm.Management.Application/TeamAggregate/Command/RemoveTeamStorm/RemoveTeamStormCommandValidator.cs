﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamStorm
{
    using FluentValidation;
    public class RemoveTeamStormCommandValidator : AbstractValidator<RemoveTeamStormCommand>
    {
        public RemoveTeamStormCommandValidator()
        {
            this.RuleFor(r => r.StormId).NotEmpty();
            this.RuleFor(r => r.TeamId).NotEmpty();
        }
    }
}
