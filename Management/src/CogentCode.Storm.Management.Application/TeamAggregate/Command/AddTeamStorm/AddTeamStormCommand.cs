﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamStorm
{
    using System;

    using MediatR;

    public class AddTeamStormCommand : IRequest
    {
        public AddTeamStormCommand(Guid teamId, Guid stormId)
        {
            this.TeamId = teamId;
            this.StormId = stormId;
        }

        public Guid StormId { get; }
        public Guid TeamId { get; set; }
    }
}
