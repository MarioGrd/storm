﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamStorm
{
    using FluentValidation;

    public class AddTeamStormCommandValidator : AbstractValidator<AddTeamStormCommand>
    {
        public AddTeamStormCommandValidator()
        {
            this.RuleFor(r => r.StormId).NotEmpty();
            this.RuleFor(r => r.TeamId).NotEmpty();
        }
    }
}
