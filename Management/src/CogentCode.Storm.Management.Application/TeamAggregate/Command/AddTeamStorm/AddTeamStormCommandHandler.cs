﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamStorm
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;
    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class AddTeamStormCommandHandler : AsyncRequestHandler<AddTeamStormCommand>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ITeamRepository teamRepository;

        public AddTeamStormCommandHandler(IUnitOfWork unitOfWork, ITeamRepository teamRepository)
        {
            this.unitOfWork = unitOfWork;
            this.teamRepository = teamRepository;
        }

        protected override async Task Handle(AddTeamStormCommand request, CancellationToken cancellationToken)
        {
            Team team = await this.teamRepository.GetTeamByIdSafeAsync(request.TeamId);

            team.AddTeamStorm(new TeamStorm(request.TeamId, request.StormId));

            this.teamRepository.UpdateTeam(team);

            await this.unitOfWork.SaveChangesAsync();
        }
    }
}
