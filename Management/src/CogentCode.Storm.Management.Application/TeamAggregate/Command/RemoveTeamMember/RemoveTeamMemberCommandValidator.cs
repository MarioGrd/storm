﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamMember
{
    using FluentValidation;
    public class RemoveTeamMemberCommandValidator : AbstractValidator<RemoveTeamMemberCommand>
    {
        public RemoveTeamMemberCommandValidator()
        {
            this.RuleFor(r => r.UserId).NotEmpty();
            this.RuleFor(r => r.TeamId).NotEmpty();
        }
    }
}
