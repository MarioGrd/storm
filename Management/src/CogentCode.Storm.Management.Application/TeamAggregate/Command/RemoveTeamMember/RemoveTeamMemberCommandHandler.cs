﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamMember
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;
    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class RemoveTeamMemberCommandHandler : AsyncRequestHandler<RemoveTeamMemberCommand>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ITeamRepository teamRepository;

        public RemoveTeamMemberCommandHandler(IUnitOfWork unitOfWork, ITeamRepository teamRepository)
        {
            this.unitOfWork = unitOfWork;
            this.teamRepository = teamRepository;
        }

        protected override async Task Handle(RemoveTeamMemberCommand request, CancellationToken cancellationToken)
        {
            Team team = await this.teamRepository.GetTeamByIdSafeAsync(request.TeamId);

            team.RemoveTeamMember(new TeamMember(request.TeamId, request.UserId));

            this.teamRepository.UpdateTeam(team);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}
