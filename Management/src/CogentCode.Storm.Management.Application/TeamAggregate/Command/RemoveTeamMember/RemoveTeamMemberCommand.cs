﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamMember
{
    using System;

    using MediatR;

    public class RemoveTeamMemberCommand : IRequest
    {
        public RemoveTeamMemberCommand(Guid teamId, Guid userId)
        {
            this.TeamId = teamId;
            this.UserId = userId;
        }

        public Guid TeamId { get; }

        public Guid UserId { get; }
    }
}
