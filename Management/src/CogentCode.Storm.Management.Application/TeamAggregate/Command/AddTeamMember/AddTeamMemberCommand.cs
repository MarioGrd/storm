﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamMember
{
    using System;

    using MediatR;

    public class AddTeamMemberCommand : IRequest
    {
        public AddTeamMemberCommand(Guid teamId, Guid userId)
        {
            this.TeamId = teamId;
            this.UserId = userId;
        }
        public Guid TeamId { get; }

        public Guid UserId { get; }
    }
}
