﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamMember
{
    using System.Threading;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;
    using CogentCode.Storm.Management.Port;

    using MediatR;

    internal class AddTeamMemberCommandHandler : AsyncRequestHandler<AddTeamMemberCommand>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ITeamRepository teamRepository;

        public AddTeamMemberCommandHandler(IUnitOfWork unitOfWork, ITeamRepository teamRepository)
        {
            this.unitOfWork = unitOfWork;
            this.teamRepository = teamRepository;
        }

        protected override async Task Handle(AddTeamMemberCommand request, CancellationToken cancellationToken)
        {
            Team team = await this.teamRepository.GetTeamByIdSafeAsync(request.TeamId);

            team.AddTeamMember(new TeamMember(request.TeamId, request.UserId));

            this.teamRepository.UpdateTeam(team);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}
