﻿namespace CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamMember
{
    using FluentValidation;

    public class AddTeamMemberCommandValidator : AbstractValidator<AddTeamMemberCommand>
    {
        public AddTeamMemberCommandValidator()
        {
            this.RuleFor(r => r.TeamId).NotEmpty();
            this.RuleFor(r => r.UserId).NotEmpty();
        }
    }
}
