﻿namespace CogentCode.Storm.Management.Application.Core.Validation
{
    using System;

    using FluentValidation;

    using Microsoft.Extensions.DependencyInjection;

    // TODO: Figure out how to make validators internal.
    public class ValidatorFactory : IValidatorFactory
    {
        private readonly IServiceProvider serviceProvider;

        public ValidatorFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IValidator? GetValidator(Type type)
        {
            object validator = this.serviceProvider.GetService(type);

            return (validator == null) ? null : validator as IValidator;
        }

        public IValidator<T> GetValidator<T>()
            => this.serviceProvider.GetService<IValidator<T>>();
    }
}
