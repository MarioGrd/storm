﻿namespace CogentCode.Storm.Management.Application.Core.Validation
{
    using System;

    using FluentValidation;
    using FluentValidation.Results;

    using Microsoft.AspNetCore.Mvc.ModelBinding;

    using Newtonsoft.Json;

    public static class ValidationHandler
    {
        public static (int code, string error) Handle(Exception exception)
        {
            if (exception is ValidationException validationException)
            {
                ModelStateDictionary dictionary = new ModelStateDictionary();

                foreach (ValidationFailure error in validationException.Errors)
                {
                    dictionary.AddModelError(error.PropertyName, error.ErrorMessage);
                }

                return (400, JsonConvert.SerializeObject(dictionary));
            }

            return (500, JsonConvert.SerializeObject(new { error = "An unexpected error occurred." }));
        }
    }
}
