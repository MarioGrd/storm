﻿namespace CogentCode.Storm.Management.Application
{
    using System;
    using System.Reflection;

    using CogentCode.Storm.Management.Application.Validation;

    using FluentValidation;

    using MediatR;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class ApplicationModule
    {
        public static IServiceCollection AddApplicationModule(
            this IServiceCollection services, IConfiguration configuration)
        {
            ApplicationSettings? settings = configuration.GetSection("Application").Get<ApplicationSettings>();

            if (settings == null)
            {
                throw new ApplicationException($"Unable to find '{nameof(ApplicationSettings)}'.");
            }

            _ = services.AddMediatR(typeof(ValidatorBehavior<,>).Assembly);
            _ = services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
            _ = services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            _ = services.AddScoped<IValidatorFactory, ValidatorFactory>();

            return services;
        }
    }

    public class ApplicationSettings
    {
        public string Url { get; set; } = string.Empty;
    }
}
