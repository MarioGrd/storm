namespace CogentCode.Storm.Management.API
{
    using System;
    using System.IO;
    using System.Reflection;

    using CogentCode.Storm.Management.API.Middlewares;
    using CogentCode.Storm.Management.Application;
    using CogentCode.Storm.Management.Infrastructure.EF;
    using CogentCode.Storm.Management.Infrastructure.SingalR;
    using CogentCode.Storm.Management.Infrastructure.Store;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationModule(this.Configuration);
            services.AddStoreModule(this.Configuration);
            services.AddSignalRModule(this.Configuration);
            services.AddEfModule(this.Configuration);
            services.AddControllers();

            services.AddSwaggerGen(config =>
            {
                config.IncludeXmlComments(
                    Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionMiddleware();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Event Storming");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            _ = app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
