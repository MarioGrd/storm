﻿namespace CogentCode.Storm.Management.API.Controllers
{
    using System.Threading.Tasks;

    using MediatR;

    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected readonly IMediator mediator;

        public BaseController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected async Task<IActionResult> ProcessAsync(IRequest request)
        {
            await this.mediator.Send(request);

            return this.NoContent();
        }

        protected async Task<IActionResult> ProcessAsync<TResult>(IRequest<TResult> request)
        {
            TResult result = await this.mediator.Send(request);

            return result == null ? this.NotFound() : (IActionResult)this.Ok(result);
        }
    }
}
