﻿namespace CogentCode.Storm.Management.API.Controllers
{
    using System;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.API.Controllers.Models.Storm;
    using CogentCode.Storm.Management.Application.StormAggregate.Command.AddNoteCommand;
    using CogentCode.Storm.Management.Application.StormAggregate.Command.CreateStormCommand;
    using CogentCode.Storm.Management.Application.StormAggregate.Command.RemoveNoteCommand;
    using CogentCode.Storm.Management.Application.StormAggregate.Command.UpdateNoteLocationCommand;

    using MediatR;

    using Microsoft.AspNetCore.Mvc;

    public class StromController : BaseController
    {
        public StromController(IMediator mediator) : base(mediator)
        {
        }

        /// <summary>
        /// Creates storming session.
        /// </summary>
        /// <param name="request">Storming session request</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateStormRequest request)
            => await this.ProcessAsync(new CreateStormCommand(request.Name, request.ProductId));

        [HttpPost("{id}/note")]
        public async Task<IActionResult> AddNoteAsync(Guid id, AddNoteRequest request)
            => await this.ProcessAsync(new AddNoteCommand(id, request.X, request.Y, request.Name, request.TypeId));

        [HttpPatch("{id}/note/{noteId}")]
        public async Task<IActionResult> UpdateNoteAsync(Guid id, Guid noteId, UpdateNoteRequest request)
            => await this.ProcessAsync(new UpdateNoteLocationCommand(id, noteId, request.X, request.Y));

        [HttpDelete("{id}/note/{noteId}")]
        public async Task<IActionResult> RemoveNoteAsync(Guid id, Guid noteId)
            => await this.ProcessAsync(new RemoveNoteCommand(id, noteId));
    }
}
