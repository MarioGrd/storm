﻿namespace CogentCode.Storm.Management.API.Controllers.Models.Team
{
    public class CreateTeamRequest
    {
        public string Name { get; set; }
    }
}
