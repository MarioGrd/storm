﻿namespace CogentCode.Storm.Management.API.Controllers.Models.Storm
{
    public class UpdateNoteRequest
    {
        public long X { get; set; }

        public long Y { get; set; }
    }
}
