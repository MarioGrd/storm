﻿namespace CogentCode.Storm.Management.API.Controllers.Models.Storm
{
    public class AddNoteRequest
    {
        public string Name { get; set; }

        public int TypeId { get; set; }

        public long X { get; set; }

        public long Y { get; set; }
    }
}
