﻿namespace CogentCode.Storm.Management.API.Controllers.Models.Storm
{
    using System;

    public class CreateStormRequest
    {
        public Guid ProductId { get; set; }

        public string Name { get; set; }
    }
}
