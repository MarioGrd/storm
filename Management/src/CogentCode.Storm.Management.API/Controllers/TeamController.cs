﻿namespace CogentCode.Storm.Management.API.Controllers
{
    using System;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.API.Controllers.Models.Team;
    using CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamMember;
    using CogentCode.Storm.Management.Application.TeamAggregate.Command.AddTeamStorm;
    using CogentCode.Storm.Management.Application.TeamAggregate.Command.CreateTeam;
    using CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamMember;
    using CogentCode.Storm.Management.Application.TeamAggregate.Command.RemoveTeamStorm;

    using MediatR;

    using Microsoft.AspNetCore.Mvc;

    public class TeamController : BaseController
    {
        public TeamController(IMediator mediator) : base(mediator)
        {
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateTeamRequest request)
            => await this.ProcessAsync(new CreateTeamCommand(request.Name));

        [HttpPost("{teamId}/member/{userId}")]
        public async Task<IActionResult> AddTeamMemberAsync(Guid teamId, Guid userId)
            => await this.ProcessAsync(new AddTeamMemberCommand(teamId, userId));

        [HttpDelete("{teamId}/member/{userId}")]
        public async Task<IActionResult> RemoveTeamMemberAsync(Guid teamId, Guid userId)
            => await this.ProcessAsync(new RemoveTeamMemberCommand(teamId, userId));

        [HttpPost("{teamId}/storm/{stormId}")]
        public async Task<IActionResult> AddTeamStormAsync(Guid teamId, Guid stormId)
            => await this.ProcessAsync(new AddTeamStormCommand(teamId, stormId));

        [HttpDelete("{teamId}/storm/{stormId}")]
        public async Task<IActionResult> RemoveTeamSormAsync(Guid teamId, Guid stormId)
            => await this.ProcessAsync(new RemoveTeamStormCommand(teamId, stormId));
    }
}
