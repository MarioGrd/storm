﻿namespace CogentCode.Storm.Management.Infrastructure.Store.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    using AutoMapper;

    using CogentCode.Seedwork;
    using CogentCode.Seedwork.EventBus;
    using CogentCode.Storm.Management.Domain.StormAggregate;
    using CogentCode.Storm.Management.Domain.StormAggregate.Events;
    using CogentCode.Storm.Management.Port;

    using EventStore.ClientAPI;

    using MediatR;

    using Microsoft.Extensions.Hosting;

    internal class StormSubscription : BackgroundService
    {
        private readonly IEventStoreConnection eventStoreConnection;
        private readonly string subscriptionName = "$ce-Storm";
        private readonly string subscriptionGroup = "gr1";
        private readonly IDomainEventDeserializer deserializer;
        private readonly IMapper mapper;
        private readonly IMediator mediator;

        private readonly Dictionary<Type, Type> eventToNotificationMap = new Dictionary<Type, Type>()
        {
            { typeof(StormCreatedEvent), typeof(StormCreatedNotification) },
            { typeof(NoteAddedEvent), typeof(NoteAddedNotification) },
            { typeof(NoteRemovedEvent), typeof(NoteRemovedNotification) },
            { typeof(NoteLocationUpdatedEvent), typeof(NoteLocationUpdatedNotification) }
        };

        public StormSubscription(
            IDomainEventDeserializer deserializer,
            IMapper mapper,
            IMediator mediator)
        {
            this.deserializer = deserializer;
            this.mapper = mapper;
            this.mediator = mediator;
            this.eventStoreConnection = EventStoreConnection.Create(
                    connectionString: "ConnectTo=tcp://admin:changeit@localhost:1113; HeartBeatTimeout=5000;",
                    builder: ConnectionSettings.Create().KeepReconnecting().KeepRetrying().UseDebugLogger().DisableTls());
        }

        private void EventRecieved(
            EventStorePersistentSubscriptionBase eventStorePersistentSubscriptionBase,
            ResolvedEvent resolvedEvent)
        {
            // TODO: Add Try catch and logging.
            Console.WriteLine("Received: " + Encoding.UTF8.GetString(resolvedEvent.Event.Data));

            byte[] @event = resolvedEvent.Event.Data;
            IDomainEvent<StormId> deserializedEvent = this.deserializer.Deserialize<StormId>(resolvedEvent.Event.EventType, @event);
            Type deserilazedEventType = deserializedEvent.GetType();

            Type? notificationType = this.eventToNotificationMap.GetValueOrDefault(deserilazedEventType);

            object notification = this.mapper.Map(deserializedEvent, deserilazedEventType, notificationType);

            this.mediator.Publish(notification);

            eventStorePersistentSubscriptionBase.Acknowledge(resolvedEvent.Event.EventId);
        }

        private void SubscriptionDropped(
            EventStorePersistentSubscriptionBase eventStorePersistentSubscriptionBase,
            SubscriptionDropReason subscriptionDropReason,
            Exception ex)
        {
            this.eventStoreConnection
                .ConnectToPersistentSubscriptionAsync(
                    this.subscriptionName,
                    this.subscriptionGroup,
                    this.EventRecieved,
                    this.SubscriptionDropped).Wait();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await this.eventStoreConnection.ConnectAsync();
            await this.eventStoreConnection
                .ConnectToPersistentSubscriptionAsync(
                    this.subscriptionName,
                    this.subscriptionGroup,
                    this.EventRecieved,
                    this.SubscriptionDropped);
        }
    }
}
