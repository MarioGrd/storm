﻿namespace CogentCode.Storm.Management.Infrastructure.Store.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    using CogentCode.Seedwork;
    using CogentCode.Seedwork.EventBus;

    using EventStore.ClientAPI;

    using Newtonsoft.Json;

    internal class StormStore : IEventStore
    {
        private readonly IEventStoreConnection connection;
        private readonly IDomainEventDeserializer deserializer;

        public StormStore(
            IEventStoreConnection connection,
            IDomainEventDeserializer deserializer)
        {
            this.connection = connection;
            this.deserializer = deserializer;
        }

        public async Task AppendEventAsync<TKey>(IDomainEvent<TKey> @event) where TKey : IKey
        {
            // TODO: In transaction.
            EventData eventData = new EventData(
                   Guid.NewGuid(),
                   @event.GetType().FullName,
                   true,
                   Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event)),
                   Encoding.UTF8.GetBytes("{}"));

            await this.connection.AppendToStreamAsync(
                @event.AggregateId.ToString(),
                ExpectedVersion.Any,
                eventData);
        }

        public async Task<IEnumerable<Event<TKey>>> ReadEventsAsync<TKey>(TKey aggregateId) where TKey : IKey
        {
            List<Event<TKey>> events = new List<Event<TKey>>();
            StreamEventsSlice currentSlice;
            long nextSliceStart = StreamPosition.Start;

            do
            {
                currentSlice = await this.connection.ReadStreamEventsForwardAsync(
                    aggregateId.ToString(),
                    nextSliceStart,
                    200,
                    false);

                if (currentSlice.Status != SliceReadStatus.Success)
                {
                    throw new ApplicationException($"Aggregate {aggregateId} not found");
                }

                nextSliceStart = currentSlice.NextEventNumber;

                foreach (ResolvedEvent resolvedEvent in currentSlice.Events)
                {
                    IDomainEvent<TKey> domainEvent = this.deserializer.Deserialize<TKey>(resolvedEvent.Event.EventType, resolvedEvent.Event.Data);

                    events.Add(new Event<TKey>(resolvedEvent.Event.EventNumber, domainEvent));
                }

            } while (!currentSlice.IsEndOfStream);

            return events;
        }
    }
}
