﻿namespace CogentCode.Storm.Management.Infrastructure.Store.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using CogentCode.Seedwork;
    using CogentCode.Seedwork.EventBus;

    internal class StormRepository<TAggregate, TKey> : IEventRepository<TAggregate, TKey>
        where TAggregate : EventAggregateRoot<TKey>
        where TKey : IKey
    {
        private readonly IEventStore eventStore;

        public StormRepository(IEventStore eventStore)
        {
            this.eventStore = eventStore;
        }

        public async Task<TAggregate> GetByIdAsync(TKey aggregateId)
        {
            IEnumerable<Event<TKey>> events = await this.eventStore.ReadEventsAsync(aggregateId);

            List<IDomainEvent<TKey>> domainEvents = events.Select(i => i.DomainEvent).ToList();

            return EventAggregateFactory.FromEvents<TAggregate, TKey>(domainEvents);
        }

        public async Task SaveAsync(TAggregate aggregate)
        {
            foreach (IDomainEvent<TKey> @event in aggregate.DomainEvents)
            {
                await this.eventStore.AppendEventAsync(@event);
            }

            aggregate.ClearDomainEvents();
        }
    }
}
