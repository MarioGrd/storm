﻿namespace CogentCode.Storm.Management.Infrastructure.Store
{
    using System;
    using System.Reflection;

    using AutoMapper;

    using CogentCode.Seedwork.EventBus;
    using CogentCode.Storm.Management.Domain.StormAggregate;
    using CogentCode.Storm.Management.Infrastructure.Store.Core;
    using CogentCode.Storm.Management.Infrastructure.Store.Profiles;

    using EventStore.ClientAPI;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class StoreModule
    {
        public static IServiceCollection AddStoreModule(
           this IServiceCollection services,
           IConfiguration configuration)
        {
            StoreSettings? settings = configuration.GetSection("Store").Get<StoreSettings>();

            if (settings == null)
            {
                throw new ApplicationException($"Unable to find '{nameof(StoreSettings)}'.");
            }

            IEventStoreConnection eventStoreConnection = EventStoreConnection.Create(
            connectionString: settings.ConnectionString,
            builder: ConnectionSettings.Create().KeepReconnecting().UseDebugLogger().DisableTls());

            eventStoreConnection.ConnectAsync().Wait();

            services.AddSingleton(eventStoreConnection);
            services.AddSingleton<IDomainEventDeserializer, JsonDomainEventDeserializer>(_ => new JsonDomainEventDeserializer(Assembly.GetAssembly(typeof(Storm)) ?? throw new ApplicationException("Invalid assembly")));
            services.AddSingleton<IEventStore, StormStore>();
            services.AddSingleton<IEventRepository<Storm, StormId>, StormRepository<Storm, StormId>>();
            services.AddAutoMapper(cfg => cfg.AddProfile(new DomainEventProfile()));
            services.AddHostedService<StormSubscription>();

            return services;
        }
    }

    public class StoreSettings
    {
        public string ConnectionString { get; set; } = string.Empty;

        public string ConnectionName { get; set; } = string.Empty;
    }
}
