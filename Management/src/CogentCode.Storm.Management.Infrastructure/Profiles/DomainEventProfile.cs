﻿namespace CogentCode.Storm.Management.Infrastructure.Store.Profiles
{
    using AutoMapper;

    using CogentCode.Storm.Management.Domain.StormAggregate.Events;
    using CogentCode.Storm.Management.Port;

    public class DomainEventProfile : Profile
    {
        public DomainEventProfile()
        {
            this.CreateMap<NoteAddedEvent, NoteAddedNotification>()
                .ForMember(m => m.Type, opt => opt.MapFrom(p => p.Type.Id))
                .ForMember(m => m.AggregateId, opt => opt.MapFrom(p => p.AggregateId.Id));

            this.CreateMap<NoteRemovedEvent, NoteRemovedNotification>()
                .ForMember(m => m.AggregateId, opt => opt.MapFrom(p => p.AggregateId.Id));

            this.CreateMap<NoteLocationUpdatedEvent, NoteLocationUpdatedNotification>()
                .ForMember(m => m.AggregateId, opt => opt.MapFrom(p => p.AggregateId.Id));

            this.CreateMap<StormCreatedEvent, StormCreatedNotification>()
                .ForMember(m => m.AggregateId, opt => opt.MapFrom(p => p.AggregateId.Id));
        }
    }
}
