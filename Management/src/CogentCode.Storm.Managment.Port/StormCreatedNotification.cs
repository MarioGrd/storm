﻿namespace CogentCode.Storm.Management.Port
{
    using System;

    using MediatR;

    public class StormCreatedNotification : INotification
    {
        public Guid AggregateId { get; set; }

        public long Timestamp { get; set; }

        public Guid TeamId { get; }

        public long CreatedAt { get; }

        public string Name { get; }
    }
}
