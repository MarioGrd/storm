﻿namespace CogentCode.Storm.Management.Port
{
    using System;
    using System.Threading.Tasks;

    using CogentCode.Storm.Management.Domain.TeamAggregate;

    public interface ITeamRepository
    {
        Task<Team> GetTeamByIdSafeAsync(Guid id);

        Task AddTeamAsync(Team team);

        void UpdateTeam(Team team);
    }
}
