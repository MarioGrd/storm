﻿namespace CogentCode.Storm.Management.Port
{
    using System;

    using MediatR;

    public class NoteRemovedNotification : INotification
    {
        public Guid AggregateId { get; set; }

        public long Timestamp { get; set; }

        public Guid NoteId { get; set; }
    }
}
