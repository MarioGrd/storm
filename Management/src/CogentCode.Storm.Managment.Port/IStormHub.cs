﻿namespace CogentCode.Storm.Management.Port
{
    using System.Threading.Tasks;

    public interface IStormHub
    {
        Task SendAsync(NoteAddedNotification notification);

        Task SendAsync(NoteRemovedNotification notification);

        Task SendAsync(NoteLocationUpdatedNotification notification);
    }
}
