﻿namespace CogentCode.Storm.Management.Port
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork
    {
        Task SaveChangesAsync(CancellationToken token = default);
    }
}
