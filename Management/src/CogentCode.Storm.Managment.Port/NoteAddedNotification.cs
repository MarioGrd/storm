﻿namespace CogentCode.Storm.Management.Port
{
    using System;

    using MediatR;

    public class NoteAddedNotification : INotification
    {
        public Guid AggregateId { get; set; }

        public long Timestamp { get; set; }

        public Guid NoteId { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }

        public long X { get; set; }

        public long Y { get; set; }
    }
}
