﻿namespace CogentCode.Storm.Management.Port
{
    using System;

    using MediatR;

    public class NoteLocationUpdatedNotification : INotification
    {
        public Guid AggregateId { get; set; }

        public long Timestamp { get; set; }

        public Guid NoteId { get; }

        public long X { get; }

        public long Y { get; }
    }
}
