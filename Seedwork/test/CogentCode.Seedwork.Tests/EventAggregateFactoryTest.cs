﻿namespace CogentCode.Seedwork.Tests
{
    using System.Collections.Generic;

    using CogentCode.Seedwork.Tests.Models.EventAggregateRoot;

    using Xunit;

    public class EventAggregateFactoryTest
    {
        [Fact]
        public void ShouldCreateInstanceOfEventAggregate()
        {
            OrderAggregate aggregate = EventAggregateFactory.FromEvents<OrderAggregate, OrderId>(new List<IDomainEvent<OrderId>>()
            {
                new OrderCreatedDomainEvent(OrderId.GetDefault(), 0, 1),
                new OrderUpdatedDomainEvent(OrderId.GetDefault(), 1, 10)
            });

            Assert.NotNull(aggregate);
            Assert.Equal(OrderId.GetDefault(), aggregate.Id);
            Assert.Equal(10, aggregate.Quantity);
            Assert.Equal(1, aggregate.Version);
        }
    }
}
