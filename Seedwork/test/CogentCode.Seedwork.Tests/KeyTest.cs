﻿namespace CogentCode.Seedwork.Tests
{
    using System;

    using CogentCode.Seedwork.Tests.Models.Entities;
    using CogentCode.Seedwork.Tests.Models.Keys;

    using Xunit;

    public class KeyTest
    {
        [Fact]
        public void IdentitiesShouldBeSame()
        {
            Guid guid = Guid.NewGuid();
            ProductId first = new ProductId(guid);
            ProductId second = new ProductId(guid);

            Assert.Equal(first.Id, second.Id);
            Assert.Equal(first, second);
            Assert.True(first == second);
            Assert.True(first.Equals(second));
            Assert.False(first != second);
        }

        [Fact]
        public void IdentitesOfEntityShouldBeSame()
        {
            ProductId productId = new ProductId(Guid.NewGuid());
            Product product = new Product(productId, "Product");

            Assert.Equal(productId, product.Id);
            Assert.True(productId.Equals(product.Id));
            Assert.True(productId == product.Id);
            Assert.Equal(productId.ToString(), product.Id.ToString());
            Assert.False(productId != product.Id);
        }
    }
}
