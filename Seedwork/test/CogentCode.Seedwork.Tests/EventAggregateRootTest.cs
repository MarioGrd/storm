﻿namespace CogentCode.Seedwork.Tests
{
    using System;
    using System.Collections.Generic;

    using CogentCode.Seedwork.Tests.Models.EventAggregateRoot;

    using Xunit;
    public class EventAggregateRootTest
    {
        [Fact]
        public void ShouldApplyEventsToAggregateSuccessfully()
        {
            OrderCreatedDomainEvent creationEvent = new OrderCreatedDomainEvent(OrderId.GetDefault(), 0, 0);
            OrderUpdatedDomainEvent updateEvent = new OrderUpdatedDomainEvent(OrderId.GetDefault(), 1, 1);

            OrderAggregate orderAggregate = new OrderAggregate(0, OrderId.GetDefault());
            orderAggregate.ClearDomainEvents();

            orderAggregate.Apply(new List<IDomainEvent<OrderId>>() { creationEvent, updateEvent });

            Assert.Equal(OrderId.GetDefault(), orderAggregate.Id);
            Assert.Equal(1, orderAggregate.Quantity);
        }
    }
}
