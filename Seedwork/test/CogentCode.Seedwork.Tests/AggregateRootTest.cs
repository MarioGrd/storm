﻿namespace CogentCode.Seedwork.Tests
{
    using CogentCode.Seedwork.Tests.Models.AggregateRoot;

    using Xunit;

    public class AggregateRootTest
    {
        [Fact]
        public void ShouldAddOrderCreatedItemToAggregate()
        {
            OrderAggregate aggregate = new OrderAggregate(OrderId.GetDefault(), 1);

            Assert.Equal(1, aggregate.DomainEvents.Count);
        }

        [Fact]
        public void ShouldAddTwoDomainEventsToAggregate()
        {
            OrderAggregate aggregate = new OrderAggregate(OrderId.GetDefault(), 1);

            aggregate.UpdateQuantity(10);

            Assert.Equal(2, aggregate.DomainEvents.Count);
        }

        [Fact]
        public void ShouldClearDomainEventsFromAggregate()
        {
            OrderAggregate aggregate = new OrderAggregate(OrderId.GetDefault(), 1);

            aggregate.ClearDomainEvents();

            Assert.Equal(0, aggregate.DomainEvents.Count);
        }
    }
}
