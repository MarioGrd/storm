namespace CogentCode.Seedwork.Tests
{
    using CogentCode.Seedwork.Tests.Models.Value;

    using Xunit;

    public class ValueObjectTest
    {
        [Fact]
        public void ValueObjectsShouldBeSame()
        {
            Item first = new Item("Item", 10);
            Item second = new Item("Item", 10);

            Assert.Equal(first, second);
            Assert.True(first.Equals(second));
            Assert.True(first == second);
            Assert.False(first != second);
        }

        [Fact]
        public void CopiedValueObjectShouldBeSame()
        {
            Item first = new Item("Item", 10);
            Item second = (Item)first.GetCopy();

            Assert.Equal(first, second);
            Assert.True(first.Equals(second));
            Assert.True(first == second);
            Assert.False(first != second);
        }

        [Fact]
        public void ValueObjectsShouldNotBeSame()
        {
            Item first = new Item("Item", 10);
            Item second = new Item("Item", 20);

            Assert.NotEqual(first, second);
            Assert.False(first.Equals(second));
            Assert.False(first == second);
            Assert.True(first != second);
        }

        [Fact]
        public void CompareWithNull()
        {
            Item? first = null;

            Assert.True(first == null);
            Assert.False(first != null);
        }

        [Fact]
        public void ComperNullableAndNotNullableValueObject()
        {
            Item? item = new Item("Item", 10);
            Item otherItem = new Item("Item", 10);

            Assert.Equal(item, otherItem);
            Assert.True(otherItem.Equals(item));
            Assert.True(item == otherItem);
            Assert.False(item != otherItem);
        }
    }
}
