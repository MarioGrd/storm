﻿namespace CogentCode.Seedwork.Tests.Models.Keys
{
    using System;

    public class ProductId : Key
    {
        public ProductId(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; }

        public override string ToString() => this.Id.ToString();
    }
}
