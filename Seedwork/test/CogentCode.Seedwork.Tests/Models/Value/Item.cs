﻿namespace CogentCode.Seedwork.Tests.Models.Value
{
    using System.Collections.Generic;

    public class Item : ValueObject
    {
        public Item(string name, int price)
        {
            this.Name = name;
            this.Price = price;
        }

        public string Name { get; }

        public int Price { get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.Name;
            yield return this.Price;
        }
    }
}
