﻿namespace CogentCode.Seedwork.Tests.Models.EventAggregateRoot
{
    using System.Collections.Generic;
    using System.Linq;

    public class OrderAggregate : EventAggregateRoot<OrderId>
    {
        public OrderAggregate(int quantity, OrderId id)
        {
            this.Id = id;
            this.Quantity = quantity;
            this.AddDomainEvent(new OrderCreatedDomainEvent(this.Id, 0, Quantity));
        }

        protected OrderAggregate(IEnumerable<IDomainEvent<OrderId>> events) : base(events)
        {
            this.Id = events.First().AggregateId;
        }

        public int Quantity { get; protected set; }

        public override OrderId Id { get; protected set; }

        public void UpdateQuantity(int quantity)
        {
            this.Quantity = quantity;
            this.AddDomainEvent(new OrderUpdatedDomainEvent(this.Id, this.Version + 1, quantity));
        }

        public void On(OrderCreatedDomainEvent @event)
        {
            this.Id = @event.AggregateId;
            this.Version = @event.AggregateVersion;
            this.Quantity = @event.Quantity;
        }

        public void On(OrderUpdatedDomainEvent @event)
        {
            this.Quantity = @event.Quantity;
        }

        public static OrderAggregate GetDefault() => new OrderAggregate(1, OrderId.GetDefault());
    }
}
