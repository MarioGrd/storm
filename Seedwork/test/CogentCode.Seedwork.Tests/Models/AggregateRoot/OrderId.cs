﻿namespace CogentCode.Seedwork.Tests.Models.AggregateRoot
{
    using System;

    public class OrderId : Key
    {
        public OrderId(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; }

        public override string ToString() => this.Id.ToString();

        public static OrderId GetDefault() => new OrderId(Guid.Empty);
    }
}
