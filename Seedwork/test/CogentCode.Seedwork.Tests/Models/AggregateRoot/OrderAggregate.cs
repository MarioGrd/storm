﻿namespace CogentCode.Seedwork.Tests.Models.AggregateRoot
{
    public class OrderAggregate : AggregateRoot<OrderId>
    {
        public OrderAggregate(OrderId id, int quantity)
        {
            this.Id = id;
            this.Quantity = quantity;
            this.AddDomainEvent(new OrderCreatedDomainEvent(this.Id, 0, Quantity));
        }

        public override OrderId Id { get; protected set; }
        public int Quantity { get; protected set; }

        public void UpdateQuantity(int quantity)
        {
            this.Quantity = quantity;
            this.AddDomainEvent(new OrderUpdatedDomainEvent(this.Id, 1, quantity));
        }

        public static OrderAggregate GetDefault() => new OrderAggregate(OrderId.GetDefault(), 1);
    }
}
