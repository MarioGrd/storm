﻿namespace CogentCode.Seedwork.Tests.Models.AggregateRoot
{
    using System;

    public class OrderCreatedDomainEvent : IDomainEvent<OrderId>
    {
        public OrderCreatedDomainEvent(
            OrderId aggregateId,
            long aggregateVersion,
            int quantity)
        {
            this.AggregateId = aggregateId;
            this.AggregateVersion = aggregateVersion;
            this.Timestamp = DateTimeOffset.UtcNow.Ticks;
            this.Quantity = quantity;
        }

        public OrderId AggregateId { get; }

        public long AggregateVersion { get; }

        public long Timestamp { get; }

        public int Quantity { get; }
    }
}
