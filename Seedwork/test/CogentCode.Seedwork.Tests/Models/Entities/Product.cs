﻿namespace CogentCode.Seedwork.Tests.Models.Entities
{
    using CogentCode.Seedwork.Tests.Models.Keys;

    public class Product : Entity<ProductId>
    {
        public Product(ProductId id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public override ProductId Id { get; protected set; }

        public string Name { get; }
    }
}
