﻿namespace CogentCode.Seedwork
{
    public interface IEntity<out TKey>
    {
        TKey Id { get; }
    }
}
