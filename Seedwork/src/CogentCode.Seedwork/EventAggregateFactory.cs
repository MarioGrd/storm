﻿namespace CogentCode.Seedwork
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public static class EventAggregateFactory
    {
        public static TEventAggregate FromEvents<TEventAggregate, TKey>(List<IDomainEvent<TKey>> events)
            where TEventAggregate : EventAggregateRoot<TKey>
            where TKey : IKey
        {
            ConstructorInfo? constructorInfo = typeof(TEventAggregate)
                    .GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
                        null,
                        new Type[] { typeof(List<IDomainEvent<TKey>>) },
                        new ParameterModifier[1]);

            if (constructorInfo == null)
            {
                throw new ArgumentException($"Unable to find suitable constructor for {typeof(TEventAggregate)}.");
            }

            TEventAggregate aggregate = (TEventAggregate)constructorInfo.Invoke(new object [] { events });

            aggregate.Apply(events);

            return aggregate;
        }
    }
}
