﻿
namespace CogentCode.Seedwork
{
    using System.Collections.Generic;

    public interface IEventAggregateRoot<TKey> : IAggregateRoot<TKey>
        where TKey : IKey
    {
        void Apply(IEnumerable<IDomainEvent<TKey>> events);

        void Apply(IDomainEvent<TKey> @event);
    }
}
