﻿namespace CogentCode.Storm.Seedwork
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public abstract class Enumeration
    {
        public string Name { get; }

        public int Id { get; }

        protected Enumeration()
        {
            this.Name = string.Empty;
        }

        protected Enumeration(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString() => this.Name ?? "";

        public static IEnumerable<T> GetAll<T>() where T : Enumeration, new()
        {
            Type type = typeof(T);
            foreach (FieldInfo info in type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly))
            {
                T instance = new T();

                if (info.GetValue(instance) is T locatedValue)
                {
                    yield return locatedValue;
                }
            }
        }

        public override bool Equals(object? obj)
        {
            if (!(obj is Enumeration otherValue))
            {
                return false;
            }

            bool typeMatches = this.GetType().Equals(obj.GetType());
            bool valueMatches = this.Id.Equals(otherValue.Id);

            return typeMatches && valueMatches;
        }

        public override int GetHashCode() => this.Id.GetHashCode();

        public static int AbsoluteDifference(Enumeration firstValue, Enumeration secondValue)
            => Math.Abs(firstValue.Id - secondValue.Id);

        public static T FromValue<T>(int value) where T : Enumeration, new()
            => Parse<T, int>(value, "value", item => item.Id == value);

        public static T FromDisplayName<T>(string displayName) where T : Enumeration, new()
            => Parse<T, string>(displayName, "display name", item => item.Name == displayName);

        public static bool HasValue<T>(int value) where T : Enumeration, new()
            => GetAll<T>().Any(p => p.Id == value);

        public static bool HasDisplayValue<T>(string displayName) where T : Enumeration, new()
            => GetAll<T>().Any(p => p.Name == displayName);

        private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration, new()
        {
            T matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                string message = string.Format("'{0}' is not a valid {1} in {2}", value, description, typeof(T));

                throw new InvalidOperationException(message);
            }

            return matchingItem;
        }
    }
}
