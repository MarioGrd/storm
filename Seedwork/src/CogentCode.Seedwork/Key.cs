﻿namespace CogentCode.Seedwork
{
    using System.Collections.Generic;

    public abstract class Key : ValueObject, IKey
    {
        public abstract override string ToString();

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.ToString();
        }
    }
}
