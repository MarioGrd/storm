﻿namespace CogentCode.Seedwork
{
    using System.Collections.Generic;

    public interface IAggregateRoot<out TKey> : IEntity<TKey>
    {
        IReadOnlyCollection<IDomainEvent<TKey>> DomainEvents { get; }

        long Version { get; }
    }
}
