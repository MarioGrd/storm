﻿namespace CogentCode.Seedwork
{
    using System.Collections.Generic;

    public abstract class AggregateRoot<TKey> : Entity<TKey>, IAggregateRoot<TKey>
        where TKey : IKey
    {
        private readonly List<IDomainEvent<TKey>> domainEvents = new List<IDomainEvent<TKey>>();

        protected AggregateRoot() { }

        protected void AddDomainEvent(IDomainEvent<TKey> @event) =>
            this.domainEvents.Add(@event);

        public void ClearDomainEvents() =>
            this.domainEvents.Clear();

        public IReadOnlyCollection<IDomainEvent<TKey>> DomainEvents { get => this.domainEvents.AsReadOnly(); }

        public long Version { get; protected set; }
    }
}
