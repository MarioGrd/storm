﻿namespace CogentCode.Seedwork
{
    public interface IDomainEvent<out TKey>
    {
        public TKey AggregateId { get; }

        long Timestamp { get; }
    }
}
