﻿namespace CogentCode.Seedwork
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public abstract class EventAggregateRoot<TKey> : AggregateRoot<TKey>, IEventAggregateRoot<TKey>
        where TKey : IKey
    {
        protected EventAggregateRoot()
        {
        }

        protected EventAggregateRoot(IEnumerable<IDomainEvent<TKey>> events)
        {
            this.Apply(events);
        }

        public void Apply(IEnumerable<IDomainEvent<TKey>> events)
        {
            foreach (IDomainEvent<TKey> @event in events)
            {
                this.Apply(@event);
            }
        }

        public void Apply(IDomainEvent<TKey> @event)
        {
            this.Mutate(@event);
        }

        private void Mutate(IDomainEvent<TKey> @event)
        {
            MethodInfo? method = this.GetOnMethod(@event);

            method?.Invoke(this, new object[] { @event });
        }

        private MethodInfo? GetOnMethod(IDomainEvent<TKey> @event)
        {
            return this.GetType().GetMethod("On", new Type[] { @event.GetType() }, new ParameterModifier[] { });
        }
    }
}
