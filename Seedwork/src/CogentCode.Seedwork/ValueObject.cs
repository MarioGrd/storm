﻿namespace CogentCode.Seedwork
{
    using System.Collections.Generic;
    using System.Linq;

    public abstract class ValueObject
    {
        private const int HighPrime = 557927;

        protected abstract IEnumerable<object> GetAtomicValues();

        public override int GetHashCode()
            => this.GetAtomicValues()
                .Select((x, i) => ((x?.GetHashCode()) ?? 0) + (HighPrime * i))
                .Aggregate((x, y) => x ^ y);

        public ValueObject GetCopy()
            => (ValueObject)this.MemberwiseClone();

        public bool Equals(ValueObject? obj)
            => !(obj is null) && obj.GetType() == this.GetType() && this.GetHashCode() == obj.GetHashCode();

        public override bool Equals(object? obj)
            => !(obj is null) && (ReferenceEquals(this, obj) || this.Equals((ValueObject)obj));

        public static bool operator ==(ValueObject? lhs, ValueObject? rhs)
        {
            if (lhs is null)
            {
                return rhs is null;
            }

            return lhs.Equals(rhs);
        }

        public static bool operator !=(ValueObject? lhs, ValueObject? rhs)
            => !(lhs == rhs);
    }
}
