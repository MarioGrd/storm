﻿namespace CogentCode.Seedwork
{
    using System.Collections.Generic;

    public abstract class Entity<TKey> : IEntity<TKey>
    {
        public abstract TKey Id { get; protected set; }

        public override bool Equals(object? obj)
            => !(obj is null) && obj.GetType() == this.GetType() && EqualityComparer<TKey>.Default.Equals(this.Id, ((Entity<TKey>)obj).Id);

        public static bool operator ==(Entity<TKey>? lhs, Entity<TKey>? rhs)
        {
            if (lhs is null && rhs is null)
            {
                return true;
            }

            if (!(lhs is null))
            {
                return lhs.Equals(rhs);
            }

            return false;
        }

        public static bool operator !=(Entity<TKey>? lhs, Entity<TKey>? rhs) => !(lhs == rhs);

        public override int GetHashCode()
        {
            if (this.Id is null)
            {
                return base.GetHashCode();
            }

            return this.Id.Equals(default(TKey)) ? base.GetHashCode() : this.GetType().GetHashCode() ^ this.Id.GetHashCode();
        }
    }
}
