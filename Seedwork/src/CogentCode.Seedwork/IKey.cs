﻿namespace CogentCode.Seedwork
{
    public interface IKey
    {
        string ToString();
    }
}
