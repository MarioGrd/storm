﻿namespace CogentCode.Seedwork
{
    using System;

    public abstract class DomainEvent<TKey> : IDomainEvent<TKey>
    {
        protected DomainEvent(TKey key)
        {
            this.AggregateId = key;
            this.Timestamp = DateTimeOffset.UtcNow.Ticks;
        }

        public TKey AggregateId { get; }

        public long Timestamp { get; }
    }
}
