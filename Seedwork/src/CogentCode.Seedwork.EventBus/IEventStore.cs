﻿namespace CogentCode.Seedwork.EventBus
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IEventStore
    {
        Task<IEnumerable<Event<TKey>>> ReadEventsAsync<TKey>(TKey aggregateId) where TKey : IKey;

        Task AppendEventAsync<TKey>(IDomainEvent<TKey> @event) where TKey : IKey;
    }
}
