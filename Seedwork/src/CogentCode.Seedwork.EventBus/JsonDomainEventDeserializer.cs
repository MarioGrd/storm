﻿namespace CogentCode.Seedwork.EventBus
{
    using System;
    using System.Reflection;
    using System.Text;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class JsonDomainEventDeserializer : IDomainEventDeserializer
    {
        private readonly Assembly assembly;

        public JsonDomainEventDeserializer(Assembly assembly)
        {
            this.assembly = assembly ?? Assembly.GetExecutingAssembly();
        }

        public IDomainEvent<TKey> Deserialize<TKey>(string type, byte[] data)
            where TKey : IKey
            => this.Deserialize<TKey>(type, Encoding.UTF8.GetString(data));

        public IDomainEvent<TKey> Deserialize<TKey>(string type, string data)
            where TKey : IKey
        {
            Type? eventType = this.assembly.GetType(type);

            if (eventType == null)
            {
                throw new ArgumentNullException($"Unable to find domain event of type: '{type}'.");
            }

            object? obj = JsonConvert.DeserializeObject(
                data,
                eventType,
                new JsonSerializerSettings
                {
                    ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                    ContractResolver = new PrivateSetterContractResolver()
                });

            if (obj == null)
            {
                throw new ArgumentNullException($"Unable to deserialize domain event of type: '{type}'.");
            }

            return (IDomainEvent<TKey>)obj;
        }
    }

    internal class PrivateSetterContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(
            MemberInfo member,
            MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);

            if (!prop.Writable)
            {
                PropertyInfo property = (PropertyInfo)member;
                prop.Writable = property.GetSetMethod(true) != null;
            }

            return prop;
        }
    }
}
