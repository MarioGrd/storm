﻿namespace CogentCode.Seedwork.EventBus
{
    using System.Threading.Tasks;

    public interface IEventRepository<TAggregate, TKey>
        where TAggregate : IEventAggregateRoot<TKey>
        where TKey : IKey
    {
        Task<TAggregate> GetByIdAsync(TKey aggregateId);

        Task SaveAsync(TAggregate aggregate);
    }
}
