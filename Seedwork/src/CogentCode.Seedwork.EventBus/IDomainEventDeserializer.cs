﻿namespace CogentCode.Seedwork.EventBus
{
    public interface IDomainEventDeserializer
    {
        IDomainEvent<TKey> Deserialize<TKey>(string type, byte[] data)
            where TKey: IKey;

        IDomainEvent<TKey> Deserialize<TKey>(string type, string data)
            where TKey : IKey;
    }
}
