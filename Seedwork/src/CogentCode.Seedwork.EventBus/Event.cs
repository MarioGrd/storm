﻿namespace CogentCode.Seedwork.EventBus
{
    public class Event<TKey>
        where TKey : IKey
    {
        public Event(
            long eventNumber,
            IDomainEvent<TKey> domainEvent)
        {
            this.EventNumber = eventNumber;
            this.DomainEvent = domainEvent;
        }

        public long EventNumber { get; }

        public IDomainEvent<TKey> DomainEvent { get; }
    }
}
